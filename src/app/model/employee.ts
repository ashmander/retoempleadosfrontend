import { Area } from "./area";
import { Country } from "./country";
import { IdentificationType } from "./identification-type";
import { Traceability } from "./traceability";

export class Employee {
    firstName: string;
    otherNames: string;
    firstLastName: string;
    secondLastName: string;
    identificationNumber: string;
    state: string;
    admissionDate: Date;
    email: string;
    country: Country;
    area: Area;
    identificationType: IdentificationType;
    traceabilities: Traceability[] ;
}