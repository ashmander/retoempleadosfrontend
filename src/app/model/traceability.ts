export class Traceability {
    id: number;
    type: string;
    registrationDate: Date;
}
