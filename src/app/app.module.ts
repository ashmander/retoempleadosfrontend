import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { EmployeesComponent } from './components/employess/employees.component';
import { HttpClientModule } from '@angular/common/http'
import { PaginationComponent } from './components/pagination/pagination.component';
import { EmployeeForm } from './components/employee-form/employee-form.component';
import { FormsModule } from '@angular/forms';
import { EmployeeInfoComponent } from './components/employee-info/employee-info.component';
import { FilterComponent } from './components/filters/filter.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EmployeesComponent,
    PaginationComponent,
    EmployeeForm,
    EmployeeInfoComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
