import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { Employee } from "src/app/model/employee";
import { EmployeeService } from "src/app/services/employee.service";
import { ModalService } from "src/app/services/modal.service";
import Swal from 'sweetalert2';

@Component({
    selector: 'employees-component',
    templateUrl: './employees.component.html'
})
export class EmployeesComponent {

    employees: Employee[] = [];
    pages: any;
    employee: Employee = new Employee();
    employeeRecived: Employee;
    isFiltered: boolean = false;
    pageObservable = new BehaviorSubject<any>([]);
    pageObserver = this.pageObservable.asObservable();

    constructor(private employeeService: EmployeeService, private activatedRoute: ActivatedRoute, private router: Router, private modalService: ModalService) {}

    ngOnInit(): void {
        this.loadEmployees();
    }

    loadEmployees(): void {
        this.activatedRoute.paramMap.subscribe(params => {
            let page: number = params.get('page') != null ? +params.get('page')! : 0;
            if(this.isFiltered) {
                this.employeeService.filterEmployees(this.employeeRecived, page).subscribe(response => {
                this.employees = response.content as Employee[];
                this.setPage(response);
            });
            } else {
                this.employeeService.getEmployees(page).subscribe(response => {
                    this.employees = response.content as Employee[];
                    this.setPage(response);
                });
            }
        })
    }

    setPage(page: any) {
        this.pages = page;
        this.pageObservable.next(page);
    }

    showInfo(employee: Employee) {
        this.employee = employee;
        this.modalService.openModalInfo();
    }

    editEmployee(email: string): void {
        this.router.navigate(['employees/update', email])
    }

    deleteEmployee(email: string): void {
        Swal.fire({
            title: 'Eliminar empleado',
            text: "Estás seguro que quieres eliminar al empleado con correo "+email+"?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, eliminar!'
          }).then((result) => {
            if (result.isConfirmed) {
                this.employeeService.deleteEmployee(email).subscribe(response => {
                    this.loadEmployees()
                    Swal.fire(
                      'Borrado!',
                      'El empleado ha sido borrado con éxito.',
                      'success'
                    )
                })
            }
          })
    }

    recibeEmployee(employee: Employee) {
        this.employeeService.filterEmployees(employee, 0).subscribe(response => {
            this.employees = response.content as Employee[];
            this.setPage(response);
            this.employeeRecived = employee;
            this.isFiltered = true;
        })
    }
}