import { Component, Input } from "@angular/core";
import { Employee } from "src/app/model/employee";
import { ModalService } from "src/app/services/modal.service";

@Component({
    selector: 'employee-info',
    templateUrl: './employee-info.component.html',
    styleUrls: ['./employee-info.component.css']
})
export class EmployeeInfoComponent {

    @Input() employee: Employee = new Employee();

    constructor(public modalService: ModalService) {}

    closeModal() {
        this.modalService.closeModalInfo();
    }
}