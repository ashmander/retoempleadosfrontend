import { Component, Input } from "@angular/core";
import { Subscription } from "rxjs";
import { EmployeesComponent } from "../employess/employees.component";

@Component({
    selector: 'pagination-component',
    templateUrl: './pagination.component.html'
})
export class PaginationComponent {

    subscription: Subscription;
    pages: any;
    
    @Input()isFiltered: boolean;

    since: number = 0;
    until: number = 0;
    
    pageNumbers:number[] = []

    constructor(private employeesComponent: EmployeesComponent) {}

    ngOnInit() {
        this.subscription = this.employeesComponent.pageObserver.subscribe(page => {
            this.pages = page;
            this.since = Math.min(Math.max(1, this.pages.number - 4), this.pages.totalPages-5);
            this.until = Math.max(Math.min(this.pages.totalPages, this.pages.number + 4), 6);
            if(this.pages.totalPages > 5) {
                this.pageNumbers = new Array(this.until - this.since).fill(0).map((value, index) => index + this.since);
            } else {
                this.pageNumbers = new Array(this.pages.totalPages).fill(0).map((value, index) => index+1);
            }
        })
    }
}