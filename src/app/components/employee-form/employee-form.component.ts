import { ThisReceiver } from "@angular/compiler";
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Area } from "src/app/model/area";
import { Country } from "src/app/model/country";
import { Employee } from "src/app/model/employee";
import { IdentificationType } from "src/app/model/identification-type";
import { AreaService } from "src/app/services/area.service";
import { CountryService } from "src/app/services/country.service";
import { EmployeeService } from "src/app/services/employee.service";
import { IdentificationTypeService } from "src/app/services/identification-type.service";
import Swal from "sweetalert2";

@Component({
    selector: 'employee-form',
    templateUrl: './employee-form.component.html'
})
export class EmployeeForm {

    employee: Employee = new Employee();
    email: string = "";
    areas: Area[] = [];
    countries: Country[] = [];
    identificationTypes: IdentificationType[] = [];

    constructor (private activatedRoute: ActivatedRoute, private employeeService: EmployeeService, private router: Router,
        private areaService: AreaService, private countryService: CountryService, private identificationTypeService: IdentificationTypeService) {}

    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(params => {
            let emailParam = params.get('email')!;
            if(emailParam !== 'undefined') {
                this.email = emailParam;
                this.employeeService.getEmployee(emailParam).subscribe(employee => this.employee = employee)
            }
            this.areaService.getAreas().subscribe(areas => this.areas = areas);
            this.countryService.getCountries().subscribe(countries => this.countries = countries)
            this.identificationTypeService.getIdentificationTypes().subscribe(identificationTypes => this.identificationTypes = identificationTypes);
        })

    }

    createEmployee(): void {
        this.employee.firstName = this.employee.firstName.toUpperCase();
        this.employee.firstLastName = this.employee.firstLastName.toUpperCase();
        this.employee.secondLastName = this.employee.secondLastName.toUpperCase();
        this.employee.otherNames = this.employee.otherNames.toUpperCase();
        console.log(this.employee)
        this.employeeService.createEmployee(this.employee).subscribe(employee => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El empleado ha sido creado con éxito',
                showConfirmButton: false,
                timer: 1500
              })
              this.router.navigate(['/employees/page/', 0])
        })
    }

    updateEmployee(): void {
        this.employee.firstName = this.employee.firstName.toUpperCase();
        this.employee.firstLastName = this.employee.firstLastName.toUpperCase();
        this.employee.secondLastName = this.employee.secondLastName.toUpperCase();
        this.employee.otherNames = this.employee.otherNames.toUpperCase();
        this.employeeService.updateEmployee(this.employee).subscribe(employee => {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'El empleado ha sido actualizado con éxito',
                showConfirmButton: false,
                timer: 1500
              })
              this.router.navigate(['/employees/page/0'])
        })
    }

    compareCountry(country1: Country, country2: Country) {
        if(country1 === undefined && country2 === undefined) {
            return true;
        }
        return country1 === null || country2 === null || country1 === undefined || country2 === undefined ? false : country1.id === country2.id;
    }

    compareArea(area1: Area, area2: Area) {
        if(area1 === undefined && area2 === undefined) {
            return true;
        }
        return area1 === null || area2 === null || area1 === undefined || area2 === undefined ? false : area1.id === area2.id;
    }

    compareIdentificationType(identificationType1: IdentificationType, identificationType2: IdentificationType) {
        if(identificationType1 === undefined && identificationType2 === undefined) {
            return true;
        }
        return identificationType1 === null || identificationType2 === null || identificationType1 === undefined || identificationType2 === undefined ? false : identificationType1.id === identificationType2.id;
    }
}
