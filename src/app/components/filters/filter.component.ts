import { Component, EventEmitter, Output } from "@angular/core";
import { Area } from "src/app/model/area";
import { Country } from "src/app/model/country";
import { Employee } from "src/app/model/employee";
import { IdentificationType } from "src/app/model/identification-type";
import { CountryService } from "src/app/services/country.service";
import { EmployeeService } from "src/app/services/employee.service";
import { IdentificationTypeService } from "src/app/services/identification-type.service";

@Component({
    selector: 'filter-component',
    templateUrl: 'filter.component.html'
})
export class FilterComponent {

    @Output()
    employeeToSend = new EventEmitter<Employee>();
    
    employee: Employee = new Employee();
    countries: Country[] = [];
    identificationTypes: IdentificationType[] = [];

    constructor (private employeeService: EmployeeService,
         private countryService: CountryService, private identificationTypeService: IdentificationTypeService) {}

    ngOnInit() {
            this.countryService.getCountries().subscribe(countries => this.countries = countries)
            this.identificationTypeService.getIdentificationTypes().subscribe(identificationTypes => this.identificationTypes = identificationTypes);

    }

    compareCountry(country1: Country, country2: Country) {
        if(country1 === undefined && country2 === undefined) {
            return true;
        }
        return country1 === null || country2 === null || country1 === undefined || country2 === undefined ? false : country1.id === country2.id;
    }

    compareIdentificationType(identificationType1: IdentificationType, identificationType2: IdentificationType) {
        if(identificationType1 === undefined && identificationType2 === undefined) {
            return true;
        }
        return identificationType1 === null || identificationType2 === null || identificationType1 === undefined || identificationType2 === undefined ? false : identificationType1.id === identificationType2.id;
    }

    filter(): void {
        this.employeeToSend.emit(this.employee);
    }
}