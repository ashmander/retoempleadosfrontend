import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeForm } from './components/employee-form/employee-form.component';
import { EmployeesComponent } from './components/employess/employees.component';

const routes: Routes = [
  {path: '', redirectTo: '/employees/page/0', pathMatch: 'full'},
  {path: 'employees/page/:page', component: EmployeesComponent},
  {path: 'employees/update/:email', component: EmployeeForm}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
