import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { URL_BACKEND } from '../config/config';
import { IdentificationType } from '../model/identification-type';

@Injectable({
  providedIn: 'root'
})
export class IdentificationTypeService {

  constructor(private http: HttpClient) { }

  getIdentificationTypes(): Observable<IdentificationType[]> {
    return this.http.get<IdentificationType[]>(URL_BACKEND+"identificationtypes");
  }
}
