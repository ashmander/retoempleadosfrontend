import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Employee } from '../model/employee';
import { URL_BACKEND } from '../config/config';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  

  constructor(private http: HttpClient) { }

  getEmployees(page: number): Observable<any> {
    return this.http.get<any>(URL_BACKEND + "employees/page/" + page);
  }

  deleteEmployee(email: string): Observable<any> {
    return this.http.delete<any>(URL_BACKEND + "employees/delete/" + email).pipe(
      catchError(e => {
        Swal.fire("Error creando el empleado", e.error.error, "error");
        return throwError(e);
      })
    );
  }

  createEmployee(employee: Employee): Observable<any> {
    return this.http.post<any>(URL_BACKEND+"employees/create", employee).pipe(
      catchError(e => {
        Swal.fire("Error creando el empleado", e.error.error, "error");
        return throwError(e);
      })
    );
  }

  updateEmployee(employee: Employee): Observable<any> {
    return this.http.put<any>(URL_BACKEND+"employees/update", employee).pipe(
      catchError(e => {
        Swal.fire("Error creando el empleado", e.error.error, "error");
        return throwError(e);
      })
    );
  }

  getEmployee(email: string): Observable<Employee> {
    return this.http.get<Employee>(URL_BACKEND+"employees/"+email).pipe(
      catchError(e => {
        Swal.fire("Error creando el empleado", e.error.error, "error");
        return throwError(e);
      })
    );
  }

  filterEmployees(employee: Employee, page: number): Observable<any> {
    return this.http.post<any>(URL_BACKEND + "employees/filter/page/" + page, employee);
  }
}
