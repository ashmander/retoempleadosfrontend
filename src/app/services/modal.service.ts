import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalInfo: boolean = false;

  constructor() { }

  openModalInfo(): void {
    this.modalInfo = true;
  }

  closeModalInfo(): void {
    this.modalInfo = false;
  }
}
